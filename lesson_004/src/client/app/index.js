// vanila script demo
var React = require('react');
var ReactDOM = require('react-dom');

var App = React.createClass({ // Warning: Accessing createClass via the main React package is deprecated, and will be removed in React v16.0. Use a plain JavaScript class instead. If you're not yet ready to migrate, create-react-class v15.* is available on npm as a temporary, drop-in replacement. For more info see https://fb.me/react-create-class
    render: function() {
        return <div>Hallo app</div>;
    }
});

var SubTag = React.createClass({ // Warning: Accessing createClass via the main React package is deprecated, and will be removed in React v16.0. Use a plain JavaScript class instead. If you're not yet ready to migrate, create-react-class v15.* is available on npm as a temporary, drop-in replacement. For more info see https://fb.me/react-create-class
    render: function() {
        return <div>Hallo tag</div>;
    }
});

ReactDOM.render(
    <div id="vanilla">
        Hello Vanilla
        <App>
            <SubTag/>
        </App>
    </div>,
    document.getElementById("app")
);
