import React, {Component} from 'react';

class ExtTimer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timer:0,
            status:0,
            counter:0,
            timerValue:"00:00.0"
        }
        this.tick = this.tick.bind(this);
    }
    startTimer(event){
        this.setState({
            status:1,
            timer:setInterval(this.tick,100)
        });
    }
    stopTimer(event){
        clearInterval(this.state.timer);
        this.setState({
            status:0,
            timer:0
        });
    }
    resetTimer(event){
        clearInterval(this.state.timer);
        this.setState({
            status:0,
            timer:0,
            counter:0,
            timerValue:"00:00.0"
        });
    }
    tick() {
        var newCounterValue = this.state.counter+1;
        this.setState({
            counter: newCounterValue,
            timerValue: this.convertToTime(newCounterValue)
        })
    }
    convertToTime(value) {
        var milisecs=value%10;
        var secs=(value-milisecs)/10%10;
        var tensecs=(value-secs*10-milisecs)/100%10%6;
        var mins=(value-tensecs*100-secs*10-milisecs)/600%10;
        var tensmins=(value-mins*600-tensecs*100-secs*10-milisecs)/6000;
        return tensmins.toFixed(0)+mins.toFixed(0)+":"+tensecs.toFixed(0)+secs+"."+milisecs;
    }
    render() {
        const startButton = <input type="button" value="Start" onClick={() => this.startTimer()}/>
        const stopButton = <input type="button" value="Stop" onClick={() => this.stopTimer()}/>
        const resetButton = <input type="button" value="Restart" onClick={() => this.resetTimer()}/>
        return <div id="extTimer">
            {this.state.status==0?startButton:""}
            {this.state.status==1?stopButton:""}
            <div>{this.state.timerValue}</div>            
            {resetButton}
        </div>
    }
}

export default ExtTimer;