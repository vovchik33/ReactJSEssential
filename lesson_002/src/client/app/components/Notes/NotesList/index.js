import React, {Component} from 'react';
import Note from '../Note';

import notesData from '../../../data/notes';

class NotesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: notesData
        };
        console.log(notesData);
    }
    render() {
        console.log(this.state.notes);
        
        return <div><div>Notes:</div><ul>
            {
                this.state.notes.map(function (item){
                    return <li>
                        <Note 
                            published={item.published} 
                            author={item.author} 
                            title={item.title} 
                            likes={item.likes}
                            email={item.email}
                            >
                            {item.description}
                        </Note>
                    </li>;
                })
            }
        </ul></div>;
    }
}

export default NotesList;