import React, {Component} from 'react';

class Note extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            title: props.title,
            description: props.children,
            author: props.author,
            email: props.email,
            published: props.published,
            likes: props.likes
        };
    }
    render() {
        return <div>
            <div className="bg-primary">{this.state.title}</div>
            <div>{this.state.published}</div>
            <div>{this.state.author}</div>
            <div>{this.state.email}</div>
            <div className="color-swatch brand-primary">{this.state.description}</div>
            <ul>
                {
                    this.state.likes.map(function(elem) {
                        return <li>{elem.social} {elem.counter}</li>;
                    })
                }
            </ul>
            <div className="container-fluid">{this.state.description}</div>
        </div>;
    }
}

export default Note;