import React, {Component} from 'react';

class PostSearch extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div id="postSearch">
            <input type="text" onChange={(event) => this.handleSearchChanged(event)}/>
        </div>
    }
    handleSearchChanged(event){
        this.props.onPostSearch(event.target.value);
    }
}

export default PostSearch