import React, {Component} from 'react';
import PostsGrid from '../PostsGrid';
import PostEditor from '../PostEditor';

import posts from '../../../data/posts';
import PostSearch from '../PostSearch';

class PostsApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: posts,
            search: ""
        };
    }

    handlePostAdded(post) {
        var newPosts = this.state.posts.slice();
        newPosts.unshift({
            id:Date.now(),
            color:post.color, 
            title:"Unknown", 
            description:post.text
        });
        this.setState({
            posts:newPosts
        });
    }   

    handlePostSearch(search) {
        this.setState({
            search:search
        });
    }

    render() {
        return <div id="postApp">
            <PostSearch onPostSearch={(search) => this.handlePostSearch(search)}/>
            <PostEditor onPostAdd={(post) => this.handlePostAdded(post)}/>
            <PostsGrid posts={this.state.posts} search={this.state.search}/>
        </div>
    }
}

export default PostsApp;