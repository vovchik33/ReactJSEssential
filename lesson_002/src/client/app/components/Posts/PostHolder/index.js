import React, {Component} from 'react';
import './styles.less';
import './rebase.css';

class PostHolder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backgroundColor: this.props.color
        }
    }
    render() {
        return <div className="post" style={{ backgroundColor: this.state.backgroundColor }}>
            <span>x</span>
            <div>{this.props.title}</div>
            <div>{this.props.children}</div>
        </div>
    }
}

export default PostHolder;