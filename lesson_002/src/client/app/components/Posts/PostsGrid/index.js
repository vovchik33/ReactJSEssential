import React, {Component} from 'react';
import PostHolder from '../PostHolder';

class PostsGrid extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.posts);
        this.state = {
            posts: this.props.posts
        }
    }
    componentDidMount() {
        var grid = this.refs.grid;
        this.msnry = new Masonry( grid, {
            itemSelector: '.post',
            columnWidth: 200,
            gutter: 5,
            isFitWidth: true
        });
    }
    componentWillReceiveProps(nextProps) {
        let search = nextProps.search;
        let posts = nextProps.posts.slice().filter(function(item){
            if (search==="") return true;
            return item.description.indexOf(search)!==-1 || item.title.indexOf(search)!==-1;
        });
        
        this.setState({
            posts: posts
        });
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.posts.length !== prevState.posts.length) {
            console.log(this.state.posts.length);
            this.msnry.reloadItems();
            this.msnry.layout();
        }
    }
    render() {
        return <div>
            <div className="postsGrid" ref="grid">
            {
                    this.state.posts.map(function(item){
                        console.log(item);
                        return <PostHolder key={item.id} title={item.title} color={item.color}>
                            {item.description}
                        </PostHolder>
                    })
            }
            </div>
        </div>
    }
}

export default PostsGrid;