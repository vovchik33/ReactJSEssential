import React, {Component} from 'react';

class PostEditor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postValue: "",
            postColor: "#990000"
        };
        this.handleAddPost = this.handleAddPost.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePost = this.handleChangePost.bind(this);
    }
    render() {
        return <div id="postEditor">
            <textarea value={this.state.postValue} onChange={this.handleChangePost}/>
            <input type="color" value={this.state.postColor} onChange={this.handleChangeColor}/>
            <input type="button" value="Add" onClick={this.handleAddPost}/>
        </div>
    }
    handleAddPost(event) {
        console.log(this.props);
        this.props.onPostAdd({
            text:this.state.postValue,
            color:this.state.postColor
        });

        this.setState({
            postValue: ""
        })
    }
    handleChangeColor(event) {
        this.setState({
            postColor: event.target.value
        });
    }
    handleChangePost(event) {
        this.setState({
            postValue: event.target.value
        });
    }
}

export default PostEditor;