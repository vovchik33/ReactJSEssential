import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './styles.less';

class Header extends Component {
    /* Component initialization */
    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
            counter: 0
        };
        this.onClickHandler = this.onClickHandler.bind(this);
    }
    /* This is only supported for classes created using React.createClass
    getDefaultProps() {
        console.log("getDefaultProps");
    }
    */
    /* This is only supported for classes created using React.createClass
     getInitialState() {
         console.log("getInitialState");
     }
     */
    componentWillMount() {
        console.log("componentWillMount");
    }
    render() {
        return <div id="header" className="header__blue-bg" onClick={this.onClickHandler}>
            <div className="header-label"> {this.props.title}</div> {this.state.clicked?"CLICKED "+this.state.counter:""}
        </div>;
    }
    componentDidMount() {
        console.log("componentDidMount");
    }

    /* Props update */
    componentWillReceiveProps() {
        console.log("componentWillReceiveProps");
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("shouldComponentUpdate");
        console.log(nextProps);
        console.log(nextState);
        return true;
    }
    componentWillUpdate(nextProps, nextState) {
        console.log("componentWillUpdate");
        console.log(nextProps);
        console.log(nextState);
    }
    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate");
        console.log(prevProps);
        console.log(prevState);
    }
    /* Destroy component */
    componentWillUnmount() {
        console.log("componentWillUnmount");
    }
    /* Handlers */
    onClickHandler(event) {
        console.log("onClick handler");
        this.setState({
            clicked: true,
            counter: this.state.counter+1
        })
        console.log(this);
        console.log(this.state);
    }
};

export default Header;

