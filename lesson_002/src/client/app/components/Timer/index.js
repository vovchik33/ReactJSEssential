import React, { Component } from 'react';

class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startTimer: new Date(),
            counter: 0
        };
        console.log(this.state);
        this.tick = this.tick.bind(this);
    }
    componentDidMount() {
        this.startTimer();
    }
    componentWillUnmount() {
        clearInterval(this.timer);
    }
    startTimer() {
        this.timer = setInterval(this.tick, 100);
    }
    tick() {
        this.setState({
            counter: new Date().getUTCSeconds() - this.state.startTimer.getUTCSeconds()
        })
    }
    render() {
        return <div>{this.props.label} {this.state.counter}</div>
    }
}

export default Timer;