import React, {Component} from 'react';
import TasksFilter from '../TasksFilter';
import TasksList from '../TasksList';
import tasks from '../../../data/tasks';

class TasksApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            filter:{
                searchString:"",
                status:0
            }
        }
    }
    componentDidMount() {
        this.setState({
            tasks: tasks
        })
    }
    render() {
        return <div id="tasksApp">
            Tasks App 
            <TasksFilter onChange={(data) => this.changedFilter(data)}/>
            <TasksList tasks={this.state.tasks} filter={this.state.filter}/>
        </div>
    }
    changedFilter(data) {
        this.setState({
            filter: data
        })
    }
}

export default TasksApp;