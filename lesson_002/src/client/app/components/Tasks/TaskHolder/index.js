import React, {Component} from 'react';

class TaskHolder extends Component {
    handleChangedStatus(event) {
        this.props.onChangeStatus({
            id:this.props.id,
            status:(event.target.checked)?'2':'1'
        });
    }
    render() {
        return <div>
            <input type="checkbox" defaultChecked={this.props.status==2?'checked':''} onChange={(event)=>this.handleChangedStatus(event)}/>
            {this.props.title} {this.props.description}
        </div>
    }
}

export default TaskHolder;