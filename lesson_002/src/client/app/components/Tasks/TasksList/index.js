import React, {Component} from 'react';
import TaskHolder from '../TaskHolder';

class TasksList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            filter: {
                searchString: "",
                status: 0
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            tasks: nextProps.tasks,
            filter: nextProps.filter
        });
    }
    handleChangedStatus(task) {
        this.state.tasks[this.state.tasks.findIndex(obj => obj.id == task.id)].status = task.status;
        this.setState({
            tasks: this.state.tasks.slice()
        });
    }
    render() {
        let filter = this.state.filter;
        let filteredTasks = this.state.tasks.filter(function(item){
            return (item.status == filter.status || filter.status == 0) 
                && (
                    !(filter.searchString && filter.searchString!=="") 
                    || (item.title.indexOf(filter.searchString)!==-1 
                    || item.description.indexOf(filter.searchString)!==-1)
                );
        });
        let self=this;
        return <div id="tasksList">
            List of tasks :
            <ul>
                {   
                    filteredTasks.map(function(item){
                        return <li key={item.id}>
                            <TaskHolder id={item.id} title={item.title} description={item.description} status={item.status} onChangeStatus={(status)=>self.handleChangedStatus(status)}/>
                        </li>
                    })
                }
            </ul>
        </div>
    }
}

export default TasksList;