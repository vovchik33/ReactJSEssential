import React, { Component } from 'react';

import './styles.less';

class TasksFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: "",
            status:0
        }
    }
    handleChangeSearch(event) {
        this.updateFilter(event.target.value, this.state.status);
    }
    handleShowAll(event) {
        this.updateFilter(this.state.searchString, 0);
    }
    handleShowNew(event) {
        this.updateFilter(this.state.searchString, 1);
    }
    handleShowCompleted(event) {
        this.updateFilter(this.state.searchString, 2);
    }

    updateFilter(searchString, status) {
        this.setState({
            searchString: searchString,
            status: status
        });
        this.props.onChange({
            searchString: searchString,
            status: status
        });  
    }
     
    render() {
        return <div id="tasksFilter" className="filter-holder">
            <input type="text" onChange={(event) => this.handleChangeSearch(event)} />
            <div >
            <a href="#" className={"filter-show-item "+((this.state.status==0)?'active':'')} onClick={(event)=>this.handleShowAll(event)}>All</a>
            <a href="#" className={"filter-show-item "+((this.state.status==1)?'active':'')} onClick={(event)=>this.handleShowNew(event)}>New</a>
            <a href="#" className={"filter-show-item "+((this.state.status==2)?'active':'')} onClick={(event)=>this.handleShowCompleted(event)}>Completed</a>
            </div>
        </div>
    }
}

export default TasksFilter;