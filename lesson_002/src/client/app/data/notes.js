export default [
    {
        id: 0,
        title: "First Note",
        description: "Simple description for the First Note",
        author: "Semen Gorbunkov",
        email: "s.gorbunkov@gmail.com",
        published: new Date().toDateString(),
        likes: [
            {social:"facebook", counter: 2},
            {social:"gmail", counter: 10}
        ]
    },
    {
        id: 1,
        title: "Second Note",
        description: "Default",
        author: "Vasilisa",
        email: "Default",
        published: new Date().toDateString(),
        likes: [
            {social:"facebook", counter: 2},
            {social:"gmail", counter: 2}
        ]
    },
    {
        id: 2,
        title: "Third Note",
        description: "Default",
        author: "Vasilisa",
        email: "Default",
        published: new Date().toDateString(),
        likes: [
            {social:"facebook", counter: 3},
            {social:"gmail", counter: 3}
        ]
    },
    {
        id: 3,
        title: "Forth Note",
        description: "Default",
        author: "Vladislav",
        email: "Default",
        published: new Date().toDateString(),
        likes: [
            {social:"facebook", counter: 4},
            {social:"gmail", counter: 4}
        ]
    },
    {
        id: 4,
        title: "Fifth Note",
        description: "Default",
        author: "Deported Man",
        email: "Default",
        published: new Date().toDateString(),
        likes: [
            {social:"facebook", counter: 5},
            {social:"gmail", counter: 5}
        ]
    }
];