import React, {Component} from 'react';
import {render} from 'react-dom';
import Header from './components/Header';
import ExtTimer from './components/ExtTimer';
import Timer from './components/Timer';
import NotesList from './components/Notes/NotesList';
import PostsApp from './components/Posts/PostsApp';

import './styles/base.less';
import TasksApp from './components/Tasks/TasksApp';

render(
    <div>
        <Header title="Header title"/>
        <TasksApp />
        {/* <ExtTimer />
        <Timer label="Timer: "/>
        <PostsApp/>
        <NotesList /> */}
    </div>,
    document.getElementById('app')
);