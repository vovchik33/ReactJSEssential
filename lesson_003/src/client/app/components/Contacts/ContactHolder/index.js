import React, {Component} from 'react';

class ContactHolder extends Component {
    render() {
        return <li key={this.props.contact.id}>{this.props.contact.firstname} {this.props.contact.lastname}</li>
    }
}

export default ContactHolder