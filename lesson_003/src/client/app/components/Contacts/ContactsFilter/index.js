import React, {Component} from 'react';
import Button from 'material-ui/Button';

class ContactsFilter extends Component {
    handleChangeFilter(event) {
        this.props.onChange(event.target.value);
    }
    render() {
        return <div id="contactsFilder">
            <input type="text" onChange={(event)=>this.handleChangeFilter(event)}/>
            <Button variant="raised" color="primary">Find</Button>
        </div>
    }
}

export default ContactsFilter