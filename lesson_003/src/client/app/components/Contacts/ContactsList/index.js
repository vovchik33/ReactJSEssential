import React, {Component} from 'react';
import ContactHolder from '../ContactHolder';

class ContactsList extends Component {
    render() {
        return <div>
            <ul>
                {
                    this.props.contacts.filter((item)=>{
                        return item.firstname.toLowerCase().indexOf(this.props.filter.toLowerCase())!==-1
                    }).map((item) => {
                        return <ContactHolder key={item.id} contact={item}/>
                    })
                }
            </ul>
        </div>
    }
}

export default ContactsList