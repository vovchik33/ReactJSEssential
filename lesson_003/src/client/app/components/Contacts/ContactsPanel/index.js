import React, {Component} from 'react';
import ContactsList from '../ContactsList';

import contacts from '../../../data/contacts';
import ContactsFilter from '../ContactsFilter';

class ContactsPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterString:""
        }
    }
    handleChangeFilter(filterString) {
        this.setState({
            filterString:filterString
        });
    }
    render(){
        return <div id="contactsPanel">
            <h3>Contacts list:</h3>
            <ContactsFilter onChange={(filterString)=>this.handleChangeFilter(filterString)}/>
            <ContactsList contacts={contacts} filter={this.state.filterString}/>
        </div>
    }
}

export default ContactsPanel