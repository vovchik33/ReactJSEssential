import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Button from "material-ui/Button";

const components = {
    first: function First(props) {
        return <div>First</div>
    },
    second: function Second(props) {
        return <div>Second</div>
    }
}

class TasksApp extends Component {
    handleClickHello(event) {
        for (var i=0;i<this.props.counter;i++) {
            console.log(this.props.title);
        }
    }
    render() {
        let Element = components[this.props.component];
        return <div>
            <Element/>
            <Button variant="raised" color="primary" onClick={(event)=>this.handleClickHello(event)}>
                {this.props.title}
            </Button>
        </div>
    }
}

TasksApp.propTypes = {
    title: PropTypes.string,
    counter: PropTypes.number
}
export default TasksApp