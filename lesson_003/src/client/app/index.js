import React, {Component} from 'react';
import ReactDOM from 'react-dom';

// import ContactsPanel from './components/Contacts/ContactsPanel';
// import TasksApp from './components/Tasks/TasksApp';
// import ArticlesApp from './components/Articles/ArticlesApp';

import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';

const Home= () => {
    return <div>Homissimo</div>
}
const About= () => {
    return <div>About</div>
}
class App extends Component {
    render() {
        let counter=3;
        return <div id="app">
            <Router>
                <div>
                    <Link to="/">Home</Link>
                    <Link to="/about">About</Link>

                    <Route exact path="/" component={Home}/>
                    <Route path="/about" component={About}/>
                </div>
            </Router>
            {/* <ContactsPanel /> */}
            {/* <TasksApp title="Hello World" counter={1} component="first"/>
            <TasksApp title="Hello World" counter={counter} component="second"/> */}
            {/* <ArticlesApp /> */}
        </div>
    }
}

ReactDOM.render(
    <div id="appContainer">
        <App/>
    </div>,
    document.getElementById("root")
)